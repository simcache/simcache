= Arquivos com instruções =

formato do arquivo

1a linha - numero de instruções do programa
2a linha -- ultima linha - instrucoes do programa

== instrucoes do programa ==

proc:instr:ender

A definição de processador segue a definição da estrutura de tipos processor_t;
As instruções seguem a definição da estrutura de tipos instructions_t;
Os endereços são endereços unsigned int;

=== Exemplo ===
A instrução

--------------------------------------------------------------------------------
sw D[%d],r4  
--------------------------------------------------------------------------------

A ser executada no processador p0 é transformada, por exemplo, em:

--------------------------------------------------------------------------------
0:1:123454
--------------------------------------------------------------------------------

