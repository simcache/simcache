\documentclass[11pt,a4paper]{article}
\usepackage{amsfonts,amssymb}
\usepackage[brazil]{babel}
\usepackage[utf8]{inputenc}

\usepackage{geometry}

\addtolength{\hoffset}{-1.5cm}
\addtolength{\textwidth}{1cm}
\addtolength{\voffset}{-1cm}
\addtolength{\textheight}{1cm}
\linespread{1}


\title{\sloppy{SimCache: Um simulador de Caches de Multiprocessadores}}
\author{Alan F. Silva \& Leslie H. Watter}


\pagestyle{plain}

\begin{document}
\maketitle{}

Neste trabalho é apresentado o simulador \textit{SimCache}. 
\textit{SimCache} é um simulador de caches para
multiprocessadores com 4 CPUs/Caches L1 conectados por meio de um barramento
único à memória RAM. 

O objetivo deste trabalho foi desenvolver um simulador de caches coerentes que
pudesse utilizar dois protocolos: um protocolo de espionagem baseado em
invalidações, e um protocolo de atualização.


\section{Descrição do simulador}
\label{sec:descr-sim}

O simulador simula as operações nas caches dos quatro processadores de acordo com a especificação
emitida e adaptações da especificação de acordo com as
dúvidas sanadas na lista de discussão e em sala de aula. 

Não foram utilizadas threads para a implementação, o que acarretou
obrigatoriamente em uma execução serial. A implementação tentou contornar a
execução serial simulando o paralelismo e deixando a simulação o mais próximo possível da
realidade.

O simulador consiste de 3 arquivos, \textrm{sim.h} e
\textrm{sim.c} contém as estruturas de dados e funções auxiliares utilizadas no
simulador e \textrm{main.c} contém a implementação do simulador propriamente dito. 

\paragraph{Execução do Simulador} O simulador inicia com todas as caches
``vazias'' com endereços / conteúdo inválido e, logo a seguir carrega e
interpreta o programa (p[1-3]\_asm) informado na linha de comando.

Caso a operação \textbf{não} seja uma operação de load/store (opcodes de 02 até 04) o número
de ciclos é computado automaticamente pela soma do resultado da função
\textit{time\_instruction}. Esta função não computa o tempo de loads e stores uma vez que
os mesmos possuem  máquina de estados com tempo de ciclo variável dependendo do
protocolo escolhido e das condições nas caches. 

O cômputo do número de ciclos para uma instrução \textrm{load} é feito da
seguinte maneira:
\begin{enumerate}
\item Se o valor já está na cache (1 ciclo para hit ou miss), caso não encontrou na cache local continua.
\item Se o valor estiver na cache, mas for inválido, significa que uma outra
  cache invalidou-o. Então deve-se fazer o write-back da cache que 
  invalidou (isso vale apenas para o protocolo de invalidação) e alterar o
  estado para shared.  
\item Se o valor esta em uma cache remota (3 ciclos para perguntar + 10 ciclos
  de transferência se estiver) 
\item Buscar o valor da memória (somente caso o valor não esteja em uma cache remota). 
\end{enumerate}

Caso seja uma instrução \textrm{store}, são executados os passos a seguir: 

\begin{enumerate}
\item Se o valor já está na cache e se é exclusivo ou modificado (1 ciclo para
  hit ou miss) 

\item Se o valor está na cache, mas seu status é \textit{shared}, pede trava
  exclusiva e: (1) muda status para modificado (caso protocolo de atualização),
  ou (2) invalida as outras cópias após a escrita (apenas para o protocolo de invalidação). 
 
\item Se o valor está na cache, mas seu status é \textit{shared}, sobrescreve o valor e manda
  broadcast para as outras cópias (apenas para o protocolo de atualização).

\item Caso o valor não esteja na cache, é buscado da memória e então sobrescrito. 

\end{enumerate}

Todas as operações são serializadas no/pelo barramento, que funciona como um vetor
e é indexado pela operação de resto (\textrm{mod ou x\%y)} do ciclo global.

Se uma posição do barramento é marcada como \textit{false}, significa que algum
processador já reservou-a e ela ficará livre apenas ao final da execução daquele
ciclo. Como o barramento é um vetor indexado pelo resto, o mesmo pode ``dar a
volta'' e por isso após a execução daquele ciclo a posição outrora ocupada é
liberada. 

Foram implementadas funções para auxiliar no controle do número de ciclos, além
do controle das posições livres no barramento.

Funções de controle de ações e número de ciclos:
\begin{itemize}
\item cache\_access
\item cache\_transfer
\item memory\_access\_transfer
\item write\_back 
\end{itemize}

Funções de controle do barramento:
\begin{itemize}
\item free\_bus\_ask (3 ciclos, atual não faz nada, próximos 2 reservados)
\item free\_bus\_transfer (8 ciclos)
\end{itemize}


O simulador pode ser executado com três programas diferentes, descritos
a seguir.


\section{Descrição dos programas de testes}
\label{sec:descr-prog-testes}

Foram desenvolvidos três programas para executar os testes do simulador. Esses programas
geram endereços aleatórios para controlar as referências a endereços/dados.

O primeiro programa gera 1000 endereços aleatórios para cada processador (é
utilizada a função \textrm{rand} para gerar os endereços e dados), o segundo
programa gera dados aleatórios para as matrizes que são de tamanho 16x16 e o
terceiro programa gera dados aleatórios para os 
caminhos mínimos da matriz de adjacências, onde 0 é a distância de um vértice
até ele mesmo e 99 é a distância quando não existe um caminho. Para os outros
vértices a distância pode variar de 1 até 20 (é utilizado mod 21 para o
resultado do rand). 

O programa 1 gera uma seqüência de $1000$ endereços aleatórios para cada CPU
presente no conjunto, onde esses endereços encontram-se na faixa de 1MBytes
($2^{20}$) com 75\% de leituras e 25\% de escritas. O programa 1 gera somente as
referências aos endereços. Todos os programas geram as referências no formato:

\begin{verbatim}
4000
00:01:738663
01:00:730054
02:00:825449
03:00:215155
\end{verbatim}

A primeira linha do arquivo gerado indica o número total de referências
presentes no arquivo. A partir da segunda linha, as referências são geradas
segundo o formato de opcode:
\begin{verbatim}
processador:operação:endereço
\end{verbatim}

As seqüências de instruções dos programas de teste foram geradas a partir de
``printf's'' das instruções a serem executadas convertidas  para o tipo de
opcode definido anteriormente.

Nas instruções load e store são utilizados endereços entre 0 e fffffff. Para as
instruções add, mul ou branch são utilizados endereços inválidos; o
endereço -2 foi utilizado para indicar um endereço inválido e -1 foi utilizado
como endereço inicial das caches.


Para cada programa de teste, ao final da saída é mostrada uma saída com as
estatísticas de cada processador, como segue:

\begin{verbatim}
number of cycles: 33929
number of cycles to P0 finish its program quarter: 31000
number of cycles that P0 lost because of the bus: 29000
number of misses in Cache0: 1000
number of coerency misses in Cache0: 0
number of cycles to P1 finish its program quarter: 30968
number of cycles that P1 lost because of the bus: 28970
number of misses in Cache1: 998
number of coerency misses in Cache1: 0
number of cycles to P2 finish its program quarter: 30972
number of cycles that P2 lost because of the bus: 28970
number of misses in Cache2: 1000
number of coerency misses in Cache2: 0
number of cycles to P3 finish its program quarter: 30970
number of cycles that P3 lost because of the bus: 28968
number of misses in Cache3: 1000
number of coerency misses in Cache3: 0
\end{verbatim}

Onde:
\begin{description}
\item[number of cycles] número de ciclos para execução completa
\item[number of cycles to PX finish its program quarter] número de ciclos
  utilizados por PX
\item[number of cycles that PX lost because of the bus] número de ciclos que PX
  ficou esperando o barramento.
\item[number of coerency misses in CacheX] número de faltas por coerência na cacheX
\end{description}




\section{Descrição e avaliação dos resultados}
\label{sec:avaliacao-resultados}

O primeiro programa obteve resultados semelhantes para ambos os protocolos
(atualização / invalidação). Na análise dos resultados, foi possível detectar que
apenas três referências são refeitas no mesmo processador e os outros 3997
endereços (1000 para o processador 0, 997 para o processador 1, 1000 para o
processador 2 e 1000 para o processador 3) são referências à dados que não estão
na cache, gerando 1000 faltas para o processador p0, 1000 faltas para o
processador p2 e  1000 faltas para o processador p3, além das 997 faltas para o
processador p1. 

A diferença entre 997 e 998 faltas no resultado do processador 1, pode ser
explicada por uma falta devido ao bloco ter sido expurgado da cache por falta de
capacidade, uma vez que a cache possui apenas 1024 blocos de 16 palavras
(16Kbytes). 

Não houve faltas por coerência detectadas, pois a análise dos resultados mostrou
que apenas 7 endereços são referenciados por processadores diferentes e nestes 7
casos a invalidação dos blocos (no caso da segunda referência ser uma escrita)
não gera faltas por coerência pois estes blocos não são referenciados uma terceira vez.

O segundo programa obteve resultados onde o protocolo de invalidação foi
mais eficiente, executando em 75\% dos ciclos utilizados pelo protocolo de
atualização.  

Isso deve-se ao programa dois ter um comportamento que acarreta poucas faltas
por coerência devido a divisão da matriz em 4 partes para a multiplicação. Em um
programa com poucas faltas por coerência, poucos blocos serão buscados  e o
número de ciclos será  menor do que o protocolo com atualização que faz
broadcast e utiliza o barramento em toda falta onde blocos são compartilhados
por mais de uma cache. 

O terceiro programa tem um comportamento que beneficia a utilização do protocolo
de atualização, que executou em  30\% menos ciclos que o protocolo de invalidação. 

Isso deve-se ao programa três ter um comportamento que acarreta muitas faltas
por coerência, além de apresentar mais faltas totais no protocolo de invalidação
do que no protocolo de atualização. Em um programa com muitas faltas por
coerência, muitos blocos serão buscados da memória e o número de ciclos será
maior do que o protocolo com atualização, uma vez que o broadcast feito  uma
única vez impede X novas faltas onde X é o número de processadores/caches que
contém uma cópia compartilhada (\textrm{shared}). 

Para os programas 2 e 3, apenas as referências aos endereços foram computadas
(posição nas matrizes), os dados não foram levados em conta.

Em relação à comparação com um único processador, os 3 programas apresentaram um desempenho
superior uma vez que cada processador demora um número de ciclos para terminar o
seu processamento muito próximo ao número de ciclos total.  

O problema nestes casos é o tempo perdido esperando pelo barramento, tempo este
que poderia acarretar em um término ainda mais rápido, uma vez que no programa
1, devido às faltas compulsórias, quase todo o tempo é utilizado esperando 
o barramento, pois o valor será buscado da memória, no programa 2 o tempo de
espera pelo barramento é $1/4$ do tempo total no protocolo de invalidação e 1/3 do
tempo total no protocolo de atualização e no programa 3 o tempo de espera pelo
barramento é 1/2 do tempo total no protocolo de invalidação e 1/4 do tempo total
no protocolo de atualização.

Se não fossem as esperas pelo barramento, o número de ciclos total de cada
programa poderia cair razoavelmente aumentando ainda mais a performance dos
multiprocessadores em relação à um único processador. 

\end{document}

% Local Variables:
% mode:tex-pdf
% mode:flyspell
% TeX-PDF-mode: t
% End:


