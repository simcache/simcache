OBJ = ./src/sim.o ./src/main.o  
HEADER = ./src/sim.h
CC = gcc -g -Wall
NOME = ./bin/sim


# Regra Principal
all : $(NOME)
	@echo " "
	@echo "              ***  Programa Instalado  *** "
	@echo "              ***  Para gerar a documentação:  *** "
	@echo "              ***  make relatorio  *** "
	@echo "              ***  Para executar os testes com saida no diretorio log:  *** "
	@echo "              ***  make test  *** "
	@echo " "

# Geracao do Executavel
$(NOME) : $(OBJ)
	if ! [ -d bin ] ; then mkdir bin ; fi
	$(CC) -o $@ $(OBJ)

# Objetos
sim.o : $(HEADER) ./src/sim.c
	$(CC) -c ./src/sim.c

main.o : $(HEADER) ./src/main.c
	$(CC) -c ./src/main.c


p1 :
	@touch ./src/p1
	@rm ./src/p1
	$(CC) ./src/prog1.c -o ./src/p1
	@./src/p1 | wc -l > ./src/p1_asm
	@./src/p1 >> ./src/p1_asm


p2 :
	@touch ./src/p2
	@rm ./src/p2
	$(CC) ./src/prog2.c -o ./src/p2
	@./src/p2 | wc -l > ./src/p2_asm
	@./src/p2 >> ./src/p2_asm

p3 :
	@touch ./src/p3
	@rm ./src/p3
	$(CC) ./src/prog3.c -o ./src/p3
	@./src/p3 | wc -l > ./src/p3_asm
	@./src/p3 >> ./src/p3_asm


test : distclean all p1 p2 p3
	if ! [ -d log ] ; then mkdir log ; fi
	@echo "Executing Program 1 Protocol Invalidation"
	@./bin/sim --prog 1 --prot 1 > log/prog1_protocol_invalidation
	@echo "Executing Program 1 Protocol Update"
	@./bin/sim --prog 1 --prot 2 > log/prog1_protocol_update
	@echo "Executing Program 2 Protocol Invalidation"
	@./bin/sim --prog 2 --prot 1 > log/prog2_protocol_invalidation
	@echo "Executing Program 2 Protocol Update"
	@./bin/sim --prog 2 --prot 2 > log/prog2_protocol_update
	@echo "Executing Program 3 Protocol Invalidation"
	@./bin/sim --prog 3 --prot 1 > log/prog3_protocol_invalidation
	@echo "Executing Program 3 Protocol Update"
	@./bin/sim --prog 3 --prot 2 > log/prog3_protocol_update
	@echo "Executing Program 0 Protocol Invalidation"

relatorio:
	@pdflatex doc/relatorio.tex  >/dev/null 2>&1
	@pdflatex doc/relatorio.tex  >/dev/null 2>&1
	@pdflatex doc/relatorio.tex  >/dev/null 2>&1
	@rm relatorio.{aux,log}
	@cp relatorio.pdf doc/
	@echo " ***  relatorio gerado em ./relatorio.pdf *** "

tags:
	ctags --recurse=yes

TAGS:
	ctags -e --recurse=yes

# Funcoes
clean : 
	@echo " "
	@echo "                    *** Limpando Sujeira... ***"
	@/bin/rm -f *.~* *.bak core* $(OBJ)

distclean : clean 
	@/bin/rm -f $(OBJ) $(NOME) 
	@echo " "
	@echo "              *** Desintalacao do Programa Concluida *** "
	@echo " "

dist : distclean
	@echo "         ***  Criando arquivo tar.tgz com o nome dos integrantes  *** "
	@cp -r ../simcache /tmp/alan_leslie
	@rm -f ../alan_leslie.tar.gz
	@tar -cz --exclude=.git -f ../alan_leslie.tar.gz  -C /tmp/ alan_leslie
	@echo "         ***  Criado arquivo alan_leslie.tar.gz em ~/scm/alan_leslie.tar.gz ***"
	@rm -rf /tmp/alan_leslie
	@echo "         *** Teste do arquivo gerado *** "
	@tar -tvz -f ../alan_leslie.tar.gz 



