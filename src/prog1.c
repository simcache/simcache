/***************************************************************************
 *   Copyright (C) 2009 by Alan Fischer e Silva / Leslie Harlley Watter    *
 *   watter@gmail.com                                                      *
 *   alanfischer85@gmail.com / alan@inf.ufpr.br                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sim.h"


#define RD 0
#define WR 1

int main (int argc, char** argv){

	unsigned int addr0,addr1,addr2,addr3; /* address0..3 */
	unsigned int mem_max=1048576; /* 2^20bytes */
	unsigned int k;
	
	for (k=0;k<1000;k++) {
		addr0=(rand()%mem_max);
		addr1=(rand()%mem_max);
		addr2=(rand()%mem_max);
		addr3=(rand()%mem_max);
		
		instructions_t inst = load;
		processor_t p = p0;
		
		switch (k%4) {
			case 0:
			{
/* 				printf("sw r1, %8x\n",  addr0); */
/* 				printf("lw r2, %8x\n",  addr1); */
/* 				printf("lw r3, %8x\n",  addr2); */
/* 				printf("lw r4, %8x\n",  addr3); */
				inst = store; 
				p=p0;
				printf("%02d:%02d:%d\n",  p,inst, addr0);
				p++;
				inst = load;
				printf("%02d:%02d:%d\n",  p,inst,addr1);
				p++;
				printf("%02d:%02d:%d\n",  p,inst,addr2);
				p++;
				printf("%02d:%02d:%d\n",  p,inst,addr3);
				break;
			}	
			case 1:
			{
/* 				printf("lw r1, %8x\n",  addr0); */
/* 				printf("sw r2, %8x\n",  addr1); */
/* 				printf("lw r3, %8x\n",  addr2); */
/* 				printf("lw r4, %8x\n",  addr3); */
				p=p0;
				inst = load;
				printf("%02d:%02d:%d\n",  p,inst,addr0);
				p++;
				inst = store;
				printf("%02d:%02d:%d\n",  p,inst,addr1);
				p++;
				inst = load;
				printf("%02d:%02d:%d\n",  p,inst,addr2);
				p++;
				printf("%02d:%02d:%d\n",  p,inst,addr3);

				break;
			}
			
			case 2:
			{
/* 				printf("lw r1, %8x\n",  addr0); */
/* 				printf("lw r2, %8x\n",  addr1); */
/* 				printf("sw r3, %8x\n",  addr2); */
/* 				printf("lw r4, %8x\n",  addr3); */
				p=p0;
				inst = load;
				printf("%02d:%02d:%d\n",  p,inst,addr0);
				p++;
				printf("%02d:%02d:%d\n",  p,inst,addr1);
				inst = store;
				p++;
				printf("%02d:%02d:%d\n",  p,inst,addr2);
				inst = load;
				p++;
				printf("%02d:%02d:%d\n",  p,inst,addr3);

				break;
			}
		
			case 3:
			{
/* 				printf("lw r1, %8x\n",  addr0); */
/* 				printf("lw r2, %8x\n",  addr1); */
/* 				printf("lw r3, %8x\n",  addr2); */
/* 				printf("sw r4, %8x\n",  addr3); */
				p=p0;
				inst = load;
				printf("%02d:%02d:%d\n",  p,inst,addr0);
				p++;
				printf("%02d:%02d:%d\n",  p,inst,addr1);
				p++;
				printf("%02d:%02d:%d\n",  p,inst,addr2);
				p++;
				inst = store;
				printf("%02d:%02d:%d\n",  p,inst,addr3);
				break;
			}
		}
		
	}


	return 0;
}
