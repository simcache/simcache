/***************************************************************************
 *   Copyright (C) 2009 by Alan Fischer e Silva / Leslie Harlley Watter    *
 *   watter@gmail.com                                                      *
 *   alanfischer85@gmail.com / alan@inf.ufpr.br                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "sim.h"

/* defines and initialize global clock */
machine_clock_t machine_clock = 0;

int main (int argc, char** argv){
	/* defines and initialize local clocks */
	machine_clock_t p0_clock = 0;
	machine_clock_t p1_clock = 0;
	machine_clock_t p2_clock = 0;
	machine_clock_t p3_clock = 0;
	
	machine_clock_t bus_clock = 0;
	
	/* defines the whole system caches */
	cache_block_t cache0[CACHE_HEIGHT];
	cache_block_t cache1[CACHE_HEIGHT];
	cache_block_t cache2[CACHE_HEIGHT];
	cache_block_t cache3[CACHE_HEIGHT];
	/* defining bus structure */
	bus_t bus[BUS_SIZE];
	
	cache_protocol_t cache_protocol = cache_invalidation;
	
	program_t *program;
	processor_fifo_t *fifo_p0, *fifo_p1, *fifo_p2, *fifo_p3;
	processor_fifo_t current_instruction_p0;
	processor_fifo_t current_instruction_p1;
	processor_fifo_t current_instruction_p2;
	processor_fifo_t current_instruction_p3;
	
	/* variables to help counting time on bus, and misses */
	unsigned int bus_count = 0;
	unsigned int lost_cycles_p0 = 0;
	unsigned int lost_cycles_p1 = 0;
	unsigned int lost_cycles_p2 = 0;
	unsigned int lost_cycles_p3 = 0;
	unsigned int misses_c0 = 0;
	unsigned int misses_c1 = 0;
	unsigned int misses_c2 = 0;
	unsigned int misses_c3 = 0;
	unsigned int coer_misses_c0 = 0;
	unsigned int coer_misses_c1 = 0;
	unsigned int coer_misses_c2 = 0;
	unsigned int coer_misses_c3 = 0;
	
	
	/* cache lookup  variables */
	unsigned int cache0_blk_addr = 0;
	unsigned int cache1_blk_addr = 0;
	unsigned int cache2_blk_addr = 0;
	unsigned int cache3_blk_addr = 0;
	bool found_p0 = false;
	bool found_p1 = false;
	bool found_p2 = false;
	bool found_p3 = false;
	bool found_c0 = false;
	bool found_c1 = false;
	bool found_c2 = false;
	bool found_c3 = false;
	
	unsigned int i = 0;
	unsigned int arg = 0;
	unsigned int top0 = 0;
	unsigned int top1 = 0;
	unsigned int top2 = 0;
	unsigned int top3 = 0;
	unsigned int size = 0;
	unsigned int quarter_size = 0;
	
	/* initializing caches with 0 because of the .h attributes */
	for (i=0;i<CACHE_HEIGHT;++i) {
			cache0[i].status = invalid;
			cache0[i].address = -1;
			
			cache1[i].status = invalid;
			cache1[i].address = -1;
			
			cache2[i].status = invalid;
			cache2[i].address = -1;
			
			cache3[i].status = invalid;
			cache3[i].address = -1;
	}
	
	/* initializing bus */
	for (i=0;i<BUS_SIZE;++i) {
		bus[i].is_free = true;	
	}
	
	if (argc == 5){
		if((strcmp(argv[1],"--prog") != 0) && (strcmp(argv[3],"--prot") != 0) \
		   && (strcmp(argv[1],"--v") != 0) && (strcmp(argv[1],"--h"))){
			printf("Wrong entry!\n");
			printf("Correct way: bin/sim [--prog][NUM][--prot][NUM]\n");
			exit(1);
		}
	}
	else if (argc != 5){
		printf("Wrong entry!\n");
		printf("Correct way: bin/sim [--prog][NUM][--prot][NUM]\n");
		exit(1);
	}
	
	/* Reading program */
	arg = atoi(argv[2]);
	
	switch ( arg ) {
		case 1: 
		{
			program = read_program("src/p1_asm", &size);
			break;
		}
		case 2:
		{
			program = read_program("src/p2_asm", &size);
			break;
		}
		
		case 3:
		{
			program = read_program("src/p3_asm", &size);
			break;
		}
		default:
		{
			printf("The program %d does not exist. Try 1, 2, or 3\n", arg);
			exit(1);
			break;
		}
		
	}
	/* space allocation */
	quarter_size = (size / 4);
	//alloc_queues(&fifo_p0, &fifo_p1, &fifo_p2, &fifo_p3, quarter_size);
	fifo_p0 = (processor_fifo_t *) malloc (sizeof(processor_fifo_t) * quarter_size);
	fifo_p1 = (processor_fifo_t *) malloc (sizeof(processor_fifo_t) * quarter_size);
	fifo_p2 = (processor_fifo_t *) malloc (sizeof(processor_fifo_t) * quarter_size);
	fifo_p3 = (processor_fifo_t *) malloc (sizeof(processor_fifo_t) * quarter_size);
	for (i = 0; i < size; i++){
		select_enqueue(program[i], fifo_p0, fifo_p1, fifo_p2, fifo_p3, &top0, &top1, &top2, &top3);
	}
	
	/* every top starts at zero and grows up to the maximum instruction number */
	top0 = top1 = top2 = top3 = 0;
	
	/* Setting protocol */
	arg = atoi(argv[4]);
	switch (arg){
		case 1:
		{
			cache_protocol = cache_invalidation;
			break;
		}
	
		case 2:
		{
			cache_protocol = cache_update;
			break;
		}
		default:
		{
			printf("The protocol %d does not exist. Try 1 or 2\n", arg);
			exit(1);
			break;
		}
		
	}
	
	/* call the processing functions of each processor (num_proc, barramento)*/
	while (( top0 < quarter_size ) || ( top1 < quarter_size ) || 
			( top2 < quarter_size ) || ( top3 < quarter_size )  || bus_count > 0 ){
		
		/* p0 */
		if ( top0 < quarter_size ) {
			if (machine_clock == p0_clock) {
				bus_clock = machine_clock;
				current_instruction_p0 = fifo_p0[top0];
				
				unqueue(fifo_p0, &top0, quarter_size);
				
				/* time for ULA instructions */
				p0_clock += time_instruction(current_instruction_p0.instr_type);
				
				/* check instruction type, if (load || store) access cache || memory */ 
				switch (current_instruction_p0.instr_type) {
					case load:
					{
						/* if (cache hit), bus_clock += (1 cicle) */
						cache0_blk_addr = current_instruction_p0.address % CACHE_HEIGHT;
						found_p0 = false;
						found_c0 = false;
						found_c1 = false;
						found_c2 = false;
						found_c3 = false;
						if (cache0[cache0_blk_addr].status != invalid ) {
							if ( cache0[cache0_blk_addr].address == current_instruction_p0.address ){
								found_p0 = true;
								found_c0 = true; /* hit */
							}
						}
						else{
							if ( cache0[cache0_blk_addr].address == current_instruction_p0.address ){
								coer_misses_c0++;
							}
						}
						/* hit or miss costs 1 cycle */
						p0_clock += 1;
						if (!found_p0) {
							
							misses_c0++;
							
							/* cache access, costs 3 cycles, takes 2 bus cycles*/
							cache_access(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, p0, current_instruction_p0);
							
							/* check if the address is in another cache */
							if (cache1[cache0_blk_addr].status != invalid ) {
								if ( cache1[cache0_blk_addr].address == current_instruction_p0.address ){
									found_p0 = true;
									found_c1 = true; 
								}
							}
							if (cache2[cache0_blk_addr].status != invalid ) {
								if ( cache2[cache0_blk_addr].address == current_instruction_p0.address ){
									found_p0 = true;
									found_c2 = true; 
								}
							}
							if (cache3[cache0_blk_addr].status != invalid ) {
								if ( cache3[cache0_blk_addr].address == current_instruction_p0.address ){
									found_p0 = true;
									found_c3 = true; 
								}
							}
							
							/*If exists in a remote cache +10 cicles, else +30*/
							if (found_p0){
								/* cache transfer */
								if ( (found_c1 && (cache1[cache0_blk_addr].status == exclusive)) ||
								     (found_c1 && (cache1[cache0_blk_addr].status == shared))    ||
								     (found_c2 && (cache2[cache0_blk_addr].status == exclusive)) ||
								     (found_c2 && (cache2[cache0_blk_addr].status == shared))    ||
								     (found_c3 && (cache3[cache0_blk_addr].status == exclusive)) ||
								     (found_c3 && (cache3[cache0_blk_addr].status == shared)) 
								   )
								{
									cache_transfer(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, p0, current_instruction_p0, cache_protocol);
								}else if ( (found_c1 && (cache1[cache0_blk_addr].status == modified))    ||
									   (found_c2 && (cache2[cache0_blk_addr].status == modified))    ||
									   (found_c3 && (cache3[cache0_blk_addr].status == modified)) 
									)
								{
										/* write back */
										write_back(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, current_instruction_p0, p0);
								}
								/* on both tranfers change the caches statuses 
								   cannot change caches status here */
								cache0[cache0_blk_addr].address = current_instruction_p0.address;
								cache0[cache0_blk_addr].status = shared;
								if (found_c1) { cache1[cache0_blk_addr].status = shared; }
								if (found_c2) { cache2[cache0_blk_addr].status = shared; }
								if (found_c3) { cache3[cache0_blk_addr].status = shared; }
							
							} else{
								/* memory access */
								memory_access_transfer(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, p0, current_instruction_p0.address);
								
								cache0[cache0_blk_addr].address = current_instruction_p0.address;
								cache0[cache0_blk_addr].status = exclusive;
							}
						} /* if (!found_p0) */
						break;
					}
					case store:
					{
						cache0_blk_addr = current_instruction_p0.address % CACHE_HEIGHT;
						found_p0 = false;
						found_c0 = false;
						found_c1 = false;
						found_c2 = false;
						found_c3 = false;
						if (cache0[cache0_blk_addr].status != invalid ) {
							if ( cache0[cache0_blk_addr].address == current_instruction_p0.address ){
								found_p0 = true;
								found_c0 = true; /* hit */
							}
						}
						else{
							if ( cache0[cache0_blk_addr].address == current_instruction_p0.address ){
								coer_misses_c0++;
							}
						}
						
						/* hit or miss costs 1 cycle */
						p0_clock += 1;
						if (found_p0) {
							if (cache0[cache0_blk_addr].status  == exclusive || cache0[cache0_blk_addr].status  == modified) {
								/* just overwrite the value without changing the status */
							}
							if (cache0[cache0_blk_addr].status  == shared) {
								if (cache_protocol == cache_invalidation){
									/* invalidate other cache lines and change local status from shared to modified */
									cache_access(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, p0, current_instruction_p0);
									
									if (cache1[cache0_blk_addr].status != invalid ) {
										if ( cache1[cache0_blk_addr].address == current_instruction_p0.address ){
											cache1[cache0_blk_addr].status = invalid;
										}
									}
									if (cache2[cache0_blk_addr].status != invalid ) {
										if ( cache2[cache0_blk_addr].address == current_instruction_p0.address ){
											cache2[cache0_blk_addr].status = invalid;
										}
									}
									if (cache3[cache0_blk_addr].status != invalid ) {
										if ( cache3[cache0_blk_addr].address == current_instruction_p0.address ){
											cache3[cache0_blk_addr].status = invalid;
										}
									}
									
									/*change the current status to exclusive */
									cache0[cache0_blk_addr].status = exclusive;
									cache0[cache0_blk_addr].address = current_instruction_p0.address;
									
								}
								else{
									/* Pay the broadcast */
									cache_transfer(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, p0, current_instruction_p0, cache_protocol);
								}
							}
						}
						
						if (!found_p0) {
							misses_c0++;
							
							/* cache access, costs 3 cycles, takes 2 bus cycles*/
							cache_access(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, p0, current_instruction_p0);
							
							if (cache1[cache0_blk_addr].status != invalid ) {
								if ( cache1[cache0_blk_addr].address == current_instruction_p0.address ){
									found_p0 = true;
									found_c1 = true;
								}
							}
							if (cache2[cache0_blk_addr].status != invalid ) {
								if ( cache2[cache0_blk_addr].address == current_instruction_p0.address ){
									found_p0 = true;
									found_c2 = true;
								}
							}
							if (cache3[cache0_blk_addr].status != invalid ) {
								if ( cache3[cache0_blk_addr].address == current_instruction_p0.address ){
									found_p0 = true;
									found_c3 = true;
								}
							}
							
							if (found_p0) {
								/* cache transfer */
								if ( (found_c1 && (cache1[cache0_blk_addr].status == exclusive)) ||
								     (found_c1 && (cache1[cache0_blk_addr].status == shared))    ||
								     (found_c2 && (cache2[cache0_blk_addr].status == exclusive)) ||
								     (found_c2 && (cache2[cache0_blk_addr].status == shared))    ||
								     (found_c3 && (cache3[cache0_blk_addr].status == exclusive)) ||
								     (found_c3 && (cache3[cache0_blk_addr].status == shared)) 
								   )
								{
									cache_transfer(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, p0, current_instruction_p0, cache_protocol);
								}else if ( (found_c1 && (cache1[cache0_blk_addr].status == modified))    ||
									   (found_c2 && (cache2[cache0_blk_addr].status == modified))    ||
									   (found_c3 && (cache3[cache0_blk_addr].status == modified)) 
									)
								{
									/* write back */
									write_back(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, current_instruction_p0, p0);
								}
								
								if (cache_protocol == cache_invalidation){
									/* on both tranfers change the caches statuses */
									cache0[cache0_blk_addr].address = current_instruction_p0.address;
									cache0[cache0_blk_addr].status = modified;
									if (found_c1) { cache1[cache0_blk_addr].status = invalid; }
									if (found_c2) { cache2[cache0_blk_addr].status = invalid; }
									if (found_c3) { cache3[cache0_blk_addr].status = invalid; }
								}
								else{
									cache0[cache0_blk_addr].address = current_instruction_p0.address;
									
									if ((!found_c1) && (!found_c2) && (!found_c3))
										cache0[cache0_blk_addr].status = modified;
									else{
										cache0[cache0_blk_addr].status = shared;
										if (found_c1) {
											cache1[cache0_blk_addr].address = current_instruction_p0.address;
											cache1[cache0_blk_addr].status = shared; 
										}
										if (found_c2) {
											cache2[cache0_blk_addr].address = current_instruction_p0.address;
											cache2[cache0_blk_addr].status = shared; 
										}
										if (found_c3) {
											cache3[cache0_blk_addr].address = current_instruction_p0.address;
											cache3[cache0_blk_addr].status = shared; 
										}
									}
								}
							} else{
								/* memory access */
								memory_access_transfer(&bus_clock, &p0_clock, &lost_cycles_p0, bus, &bus_count, p0, current_instruction_p0.address);
								
								cache0[cache0_blk_addr].address = current_instruction_p0.address;
								cache0[cache0_blk_addr].status = exclusive;
								
							}
						} /* if (!found_p0) */
						break;
					}
					case add:
					case mul:
					case branch:
					{ /* do nothing, treated before */
							break;
					}
				}/* end switch */
			}
		}
		
		/* p1 */
		if ( top1 < quarter_size ) {
			if (machine_clock == p1_clock) {
				bus_clock = machine_clock;
				current_instruction_p1 = fifo_p1[top1];
				unqueue(fifo_p1, &top1, quarter_size);
				
				/* time for ULA instructions */
				p1_clock += time_instruction(current_instruction_p1.instr_type);
				
				/* check instruction type, if (load || store) access cache || memory */ 
				switch (current_instruction_p1.instr_type) {
					case load:
					{
						/* if (cache hit), bus_clock += (1 cicle) */
						cache1_blk_addr = current_instruction_p1.address % CACHE_HEIGHT;
						found_p1 = false;
						found_c0 = false;
						found_c1 = false;
						found_c2 = false;
						found_c3 = false;
						if (cache1[cache1_blk_addr].status != invalid ) {
							if ( cache1[cache1_blk_addr].address == current_instruction_p1.address ){
								found_p1 = true;
								found_c1 = true; /* hit */
							}
						}
						else{
							if ( cache1[cache1_blk_addr].address == current_instruction_p1.address ){
								coer_misses_c1++;
							}
						}
						
						/* hit or miss costs 1 cycle */
						p1_clock += 1;
						if (!found_p1) {
							
							misses_c1++;
							
							/* cache access, costs 3 cycles, takes 2 bus cycles*/
							cache_access(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, p1, current_instruction_p1);
							
							/* check if the address is in another cache */
							if (cache0[cache1_blk_addr].status != invalid ) {
								if ( cache0[cache1_blk_addr].address == current_instruction_p1.address ){
									found_p1 = true;
									found_c0 = true; 
								}
							}
							if (cache2[cache1_blk_addr].status != invalid ) {
								if ( cache2[cache1_blk_addr].address == current_instruction_p1.address ){
									found_p1 = true;
									found_c2 = true; 
								}
							}							
							if (cache3[cache1_blk_addr].status != invalid ) {
								if ( cache3[cache1_blk_addr].address == current_instruction_p1.address ){
									found_p1 = true;
									found_c3 = true; 
								}
							}
							
							/*If exists in a remote cache +10 cicles, else +30*/
							if (found_p1){
								/* cache transfer */
								if ( (found_c0 && (cache0[cache1_blk_addr].status == exclusive)) ||
								     (found_c0 && (cache0[cache1_blk_addr].status == shared))    ||
								     (found_c2 && (cache2[cache1_blk_addr].status == exclusive)) ||
								     (found_c2 && (cache2[cache1_blk_addr].status == shared))    ||
								     (found_c3 && (cache3[cache1_blk_addr].status == exclusive)) ||
								     (found_c3 && (cache3[cache1_blk_addr].status == shared)) 
								   )
								{
									cache_transfer(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, p1, current_instruction_p1, cache_protocol);
								}else if ( (found_c0 && (cache0[cache1_blk_addr].status == modified))    ||
									   (found_c2 && (cache2[cache1_blk_addr].status == modified))    ||
									   (found_c3 && (cache3[cache1_blk_addr].status == modified)) 
									)
								{
										write_back(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, current_instruction_p1, p1);
								}
								/* on both tranfers change the caches statuses */
								cache1[cache1_blk_addr].address = current_instruction_p1.address;
								cache1[cache1_blk_addr].status = shared;
								if (found_c0) { cache0[cache1_blk_addr].status = shared; }
								if (found_c2) { cache2[cache1_blk_addr].status = shared; }
								if (found_c3) { cache3[cache1_blk_addr].status = shared; }
								
							} else{
								/* memory access */
								memory_access_transfer(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, p1, current_instruction_p1.address);
								
								cache1[cache1_blk_addr].address = current_instruction_p1.address;
								cache1[cache1_blk_addr].status = exclusive;
								
							}
						} /* if (!found_p1)*/
						break;
					}
					
					case store:
					{
						/* if (cache hit), bus_clock += (1 cicle) */
						cache1_blk_addr = current_instruction_p1.address % CACHE_HEIGHT;
						found_p1 = false;
						found_c0 = false;
						found_c1 = false;
						found_c2 = false;
						found_c3 = false;
						if (cache1[cache1_blk_addr].status != invalid ) {
							if ( cache1[cache1_blk_addr].address == current_instruction_p1.address ){
								found_p1 = true;
								found_c1 = true; /* hit */
							}
						}
						else{
							if ( cache1[cache1_blk_addr].address == current_instruction_p1.address ){
								coer_misses_c1++;
							}
						}
						
						/* hit or miss costs 1 cycle */
						p1_clock += 1;
						if (found_p1) {
							if (cache1[cache1_blk_addr].status  == exclusive || cache1[cache1_blk_addr].status  == modified) {
								/* just overwrite the value without changing the status */
							}
							if (cache1[cache1_blk_addr].status  == shared) {
								if (cache_protocol == cache_invalidation){
									/* invalidate other cache lines and change local status from shared to modified */
									cache_access(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, p1, current_instruction_p1);
									
									if (cache0[cache1_blk_addr].status != invalid ) {
										if ( cache0[cache1_blk_addr].address == current_instruction_p1.address ){
											cache0[cache1_blk_addr].status = invalid;
										}
									}
									if (cache2[cache1_blk_addr].status != invalid ) {
										if ( cache2[cache1_blk_addr].address == current_instruction_p1.address ){
											cache2[cache1_blk_addr].status = invalid;
										}
									}
									if (cache3[cache1_blk_addr].status != invalid ) {
										if ( cache3[cache1_blk_addr].address == current_instruction_p1.address ){
										cache3[cache1_blk_addr].status = invalid;
										}
									}
								
								
									/* change the current status to exclusive */
									cache1[cache1_blk_addr].status = exclusive;
									cache1[cache1_blk_addr].address = current_instruction_p1.address;
									
								}
								else{
									/* Pay the broadcast */
									cache_transfer(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, p1, current_instruction_p1, cache_protocol);
								}
							}
						}
						
						if (!found_p1) {
							
							misses_c1++;
							
							/* cache access, costs 3 cycles, takes 2 bus cycles*/
							cache_access(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, p1, current_instruction_p1);
							
							if (cache0[cache1_blk_addr].status != invalid ) {
								if ( cache0[cache1_blk_addr].address == current_instruction_p1.address ){
									found_p1 = true;
									found_c0 = true;
								}
							}
							if (cache2[cache1_blk_addr].status != invalid ) {
								if ( cache1[cache1_blk_addr].address == current_instruction_p1.address ){
									found_p1 = true;
									found_c2 = true;
								}
							}
							if (cache3[cache1_blk_addr].status != invalid ) {
								if ( cache1[cache1_blk_addr].address == current_instruction_p1.address ){
									found_p1 = true;
									found_c3 = true;
								}
							}
							
							if (found_p1) {
								/* cache transfer */
								if ( (found_c0 && (cache0[cache1_blk_addr].status == exclusive)) ||
								     (found_c0 && (cache0[cache1_blk_addr].status == shared))    ||
								     (found_c2 && (cache2[cache1_blk_addr].status == exclusive)) ||
								     (found_c2 && (cache2[cache1_blk_addr].status == shared))    ||
								     (found_c3 && (cache3[cache1_blk_addr].status == exclusive)) ||
								     (found_c3 && (cache3[cache1_blk_addr].status == shared)) 
								   )								{
									cache_transfer(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, p1, current_instruction_p1, cache_protocol);
								}else if ( (found_c0 && (cache0[cache1_blk_addr].status == modified))    ||
									   (found_c2 && (cache2[cache1_blk_addr].status == modified))    ||
									   (found_c3 && (cache3[cache1_blk_addr].status == modified)) 
									)
								{
										/* write back */
										write_back(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, current_instruction_p1, p1);
								}
								
								if (cache_protocol == cache_invalidation){
									/* on both tranfers change the caches statuses */
									cache1[cache1_blk_addr].address = current_instruction_p1.address;
									cache1[cache1_blk_addr].status = modified;
									if (found_c0) { cache0[cache1_blk_addr].status = invalid; }
									if (found_c2) { cache2[cache1_blk_addr].status = invalid; }
									if (found_c3) { cache3[cache1_blk_addr].status = invalid; }
								}
								else{
									cache1[cache1_blk_addr].address = current_instruction_p1.address;
									
									if ((!found_c0) && (!found_c2) && (!found_c3))
										cache1[cache1_blk_addr].status = modified;
									else{
										cache1[cache1_blk_addr].status = shared;
										if (found_c0) {
											cache0[cache1_blk_addr].address = current_instruction_p1.address;
											cache0[cache1_blk_addr].status = shared; 
										}
										if (found_c2) {
											cache2[cache1_blk_addr].address = current_instruction_p1.address;
											cache2[cache1_blk_addr].status = shared; 
										}
										if (found_c3) {
											cache3[cache1_blk_addr].address = current_instruction_p1.address;
											cache3[cache1_blk_addr].status = shared; 
										}
									}
								}
								
							} else{
								/* memory access */
								memory_access_transfer(&bus_clock, &p1_clock, &lost_cycles_p1, bus, &bus_count, p1, current_instruction_p1.address);
								
								cache1[cache1_blk_addr].address = current_instruction_p1.address;
								cache1[cache1_blk_addr].status = exclusive;
								
							}
						} /* if (!found_p1) */
						break;
					}
					case add:
					case mul:
					case branch:
					{ /* do nothing, treated before */
							break;
					}
				}/* end switch */
			}
		}
		
		/* p2 */
		if ( top2 < quarter_size ) {
			if (machine_clock == p2_clock) {
				
				bus_clock = machine_clock;
				current_instruction_p2 = fifo_p2[top2];
				
				unqueue(fifo_p2, &top2, quarter_size);
				
				/* time for ULA instructions */
				p2_clock += time_instruction(current_instruction_p2.instr_type);
				
				/* check instruction type, if (load || store) access cache || memory */ 
				switch (current_instruction_p2.instr_type) {
					case load:
					{
						/* if (cache hit), bus_clock += (1 cicle) */
						cache2_blk_addr = current_instruction_p2.address % CACHE_HEIGHT;
						found_p2 = false;
						found_c0 = false;
						found_c1 = false;
						found_c2 = false;
						found_c3 = false;
						if (cache2[cache2_blk_addr].status != invalid ) {
							if ( cache2[cache2_blk_addr].address == current_instruction_p2.address ){
								found_p2 = true;
								found_c2 = true; /* hit */
							}
						}
						else{
							if ( cache2[cache2_blk_addr].address == current_instruction_p2.address ){
								coer_misses_c2++;
							}
						}
						
						/* hit or miss costs 1 cycle */
						p2_clock += 1;
						if (!found_p2) {
							
							misses_c2++;
							
							/* cache access, costs 3 cycles, takes 2 bus cycles*/
							cache_access(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, p2, current_instruction_p2);
							
							/* check if the address is in another cache */
							if (cache1[cache2_blk_addr].status != invalid ) {
								if ( cache1[cache2_blk_addr].address == current_instruction_p2.address ){
									found_p2 = true;
									found_c1 = true; 
								}
							}
							if (cache0[cache2_blk_addr].status != invalid ) {
								if ( cache0[cache2_blk_addr].address == current_instruction_p2.address ){
									found_p2 = true;
									found_c0 = true; 
								}
							}							
							if (cache3[cache2_blk_addr].status != invalid ) {
								if ( cache3[cache2_blk_addr].address == current_instruction_p2.address ){
									found_p2 = true;
									found_c3 = true; 
								}
							}
							
							/*If exists in a remote cache +10 cicles, else +30*/
							if (found_p2){
								/* cache transfer */
								if ( (found_c1 && (cache1[cache2_blk_addr].status == exclusive)) ||
								     (found_c1 && (cache1[cache2_blk_addr].status == shared))    ||
								     (found_c0 && (cache0[cache2_blk_addr].status == exclusive)) ||
								     (found_c0 && (cache0[cache2_blk_addr].status == shared))    ||
								     (found_c3 && (cache3[cache2_blk_addr].status == exclusive)) ||
								     (found_c3 && (cache3[cache2_blk_addr].status == shared)) 
								   )
								{
									cache_transfer(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, p2, current_instruction_p2, cache_protocol);
								}else if ( (found_c1 && (cache1[cache2_blk_addr].status == modified))    ||
									   (found_c0 && (cache0[cache2_blk_addr].status == modified))    ||
									   (found_c3 && (cache3[cache2_blk_addr].status == modified)) 
									)
								{
									write_back(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, current_instruction_p2, p2);
								}
								/* on both tranfers change the caches statuses */
								cache2[cache2_blk_addr].address = current_instruction_p2.address;
								cache2[cache2_blk_addr].status = shared;
								if (found_c1) { cache1[cache2_blk_addr].status = shared; }
								if (found_c0) { cache0[cache2_blk_addr].status = shared; }
								if (found_c3) { cache3[cache2_blk_addr].status = shared; }
								
							}else{
								/* memory access */
								memory_access_transfer(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, p2, current_instruction_p2.address);
								
								cache2[cache2_blk_addr].address = current_instruction_p2.address;
								cache2[cache2_blk_addr].status = exclusive;
								
							}
						} /* if(!found_p2) */
						break;
					}
					case store:
					{
						/* if (cache hit), bus_clock += (1 cicle) */
						cache2_blk_addr = current_instruction_p2.address % CACHE_HEIGHT;
						found_p2 = false;
						found_c0 = false;
						found_c1 = false;
						found_c2 = false;
						found_c3 = false;
						if (cache2[cache2_blk_addr].status != invalid ) {
							if ( cache2[cache2_blk_addr].address == current_instruction_p2.address ){
								found_p2 = true;
								found_c2 = true; /* hit */
							}
						}
						else{
							if ( cache2[cache2_blk_addr].address == current_instruction_p2.address ){
								coer_misses_c2++;
							}
						}
						
						/* hit or miss costs 1 cycle */
						p2_clock += 1;
						if (found_p2) {
							if (cache2[cache2_blk_addr].status  == exclusive || cache2[cache2_blk_addr].status  == modified) {
								/* just overwrite the value without changing the status */
							}
							if (cache2[cache2_blk_addr].status  == shared) {
								if (cache_protocol == cache_invalidation){
									/* invalidate other cache lines and change local status from shared to modified */
									cache_access(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, p2, current_instruction_p2);
									
									if (cache1[cache2_blk_addr].status != invalid ) {
										if ( cache1[cache2_blk_addr].address == current_instruction_p2.address ){
											cache1[cache2_blk_addr].status = invalid;
										}
									}
									if (cache0[cache2_blk_addr].status != invalid ) {
										if ( cache0[cache2_blk_addr].address == current_instruction_p2.address ){
											cache0[cache2_blk_addr].status = invalid;
										}
									}
									if (cache3[cache2_blk_addr].status != invalid ) {
										if ( cache3[cache2_blk_addr].address == current_instruction_p2.address ){
											cache3[cache2_blk_addr].status = invalid;
										}
									}
									
									/* change the current status to exclusive */
									cache2[cache2_blk_addr].status = exclusive;
									cache2[cache2_blk_addr].address = current_instruction_p2.address;
									
								}
								else{
									/* Pay the broadcast */
									cache_transfer(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, p2, current_instruction_p2, cache_protocol);
								}
							}
						}
						
						if (!found_p2) {
							
							misses_c2++;
							
							/* cache access, costs 3 cycles, takes 2 bus cycles*/
							cache_access(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, p2, current_instruction_p2);
							
							if (cache1[cache2_blk_addr].status != invalid ) {
								if ( cache1[cache2_blk_addr].address == current_instruction_p2.address ){
									found_p2 = true;
									found_c1 = true;
								}
							}
							if (cache0[cache2_blk_addr].status != invalid ) {
								if ( cache0[cache2_blk_addr].address == current_instruction_p2.address ){
									found_p2 = true;
									found_c0 = true;
								}
							}
							if (cache3[cache2_blk_addr].status != invalid ) {
								if ( cache3[cache2_blk_addr].address == current_instruction_p2.address ){
									found_p2 = true;
									found_c3 = true;
								}
							}
							
							if (found_p2) {
								/* cache transfer */
								if ( (found_c1 && (cache1[cache2_blk_addr].status == exclusive)) ||
								     (found_c1 && (cache1[cache2_blk_addr].status == shared))    ||
								     (found_c0 && (cache0[cache2_blk_addr].status == exclusive)) ||
								     (found_c0 && (cache0[cache2_blk_addr].status == shared))    ||
								     (found_c3 && (cache3[cache2_blk_addr].status == exclusive)) ||
								     (found_c3 && (cache3[cache2_blk_addr].status == shared)) 
								   )
								{
									cache_transfer(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, p2, current_instruction_p2, cache_protocol);
								}else if ( (found_c1 && (cache1[cache2_blk_addr].status == modified))    ||
									   (found_c0 && (cache0[cache2_blk_addr].status == modified))    ||
									   (found_c3 && (cache3[cache2_blk_addr].status == modified)) 
									)
								{
									write_back(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, current_instruction_p2, p2);
								}
								
								if (cache_protocol == cache_invalidation){
									/* on both tranfers change the caches statuses */
									cache2[cache2_blk_addr].address = current_instruction_p2.address;
									cache2[cache2_blk_addr].status = modified;
									if (found_c0) { cache0[cache2_blk_addr].status = invalid; }
									if (found_c1) { cache1[cache2_blk_addr].status = invalid; }
									if (found_c3) { cache3[cache2_blk_addr].status = invalid; }
								}
								else{
									cache2[cache2_blk_addr].address = current_instruction_p2.address;
									
									if ((!found_c0) && (!found_c1) && (!found_c3))
										cache1[cache2_blk_addr].status = modified;
									else{
										cache2[cache2_blk_addr].status = shared;
										if (found_c0) {
											cache0[cache2_blk_addr].address = current_instruction_p2.address;
											cache0[cache2_blk_addr].status = shared; 
										}
										if (found_c1) {
											cache2[cache2_blk_addr].address = current_instruction_p2.address;
											cache2[cache2_blk_addr].status = shared; 
										}
										if (found_c3) {
											cache3[cache2_blk_addr].address = current_instruction_p2.address;
											cache3[cache2_blk_addr].status = shared; 
										}
									}
								}
							} else{
								/* memory access */
								memory_access_transfer(&bus_clock, &p2_clock, &lost_cycles_p2, bus, &bus_count, p2, current_instruction_p2.address);
								
								cache2[cache2_blk_addr].address = current_instruction_p2.address;
								cache2[cache2_blk_addr].status = exclusive;
								
							}
						} /* if (!found_p2) */
						break;
					}
					case add:
					case mul:
					case branch:
					{ /* do nothing, treated before */
							break;
					}
				}/* end switch */
			}
		}
			
		/* p3 */
		if ( top3 < quarter_size ) {
			if (machine_clock == p3_clock) {
				
				bus_clock = machine_clock;
				current_instruction_p3 = fifo_p3[top3];
				unqueue(fifo_p3, &top3, quarter_size);
				
				/* time for ULA instructions */
				p3_clock += time_instruction(current_instruction_p3.instr_type);
				
				/* check instruction type, if (load || store) access cache || memory */
				switch (current_instruction_p3.instr_type) {
					case load:
					{
						/* if (cache hit), bus_clock += (1 cicle) */
						cache3_blk_addr = current_instruction_p3.address % CACHE_HEIGHT;
						found_p3 = false;
						found_c0 = false;
						found_c1 = false;
						found_c2 = false;
						found_c3 = false;
						if (cache3[cache3_blk_addr].status != invalid ) {
							if ( cache3[cache3_blk_addr].address == current_instruction_p3.address ){
								found_p3 = true;
								found_c3 = true; /* hit */
							}
						}
						else{
							if ( cache3[cache3_blk_addr].address == current_instruction_p3.address ){
								coer_misses_c3++;
							}
						}
						
						/* hit or miss costs 1 cycle */
						p3_clock += 1;
						if (!found_p3) {
							
							misses_c3++;
							
							/* cache access, costs 3 cycles, takes 2 bus cycles*/
							cache_access(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, p3, current_instruction_p3);
							
							/* check if the address is in another cache */
							if (cache1[cache3_blk_addr].status != invalid ) {
								if ( cache1[cache3_blk_addr].address == current_instruction_p3.address ){
									found_p3 = true;
									found_c1 = true; 
								}
							}
							if (cache2[cache3_blk_addr].status != invalid ) {
								if ( cache2[cache3_blk_addr].address == current_instruction_p3.address ){
									found_p3 = true;
									found_c2 = true; 
								}
							}							
							if (cache0[cache3_blk_addr].status != invalid ) {
								if ( cache0[cache3_blk_addr].address == current_instruction_p3.address ){
									found_p3 = true;
									found_c0 = true; 
								}
							}
							
							/*If exists in a remote cache +10 cicles, else +30*/
							if (found_p3){
								/* cache transfer */
								if ( (found_c1 && (cache1[cache3_blk_addr].status == exclusive)) ||
								     (found_c1 && (cache1[cache3_blk_addr].status == shared))    ||
								     (found_c2 && (cache2[cache3_blk_addr].status == exclusive)) ||
								     (found_c2 && (cache2[cache3_blk_addr].status == shared))    ||
								     (found_c0 && (cache0[cache3_blk_addr].status == exclusive)) ||
								     (found_c0 && (cache0[cache3_blk_addr].status == shared)) 
								   )
								{
									cache_transfer(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, p3, current_instruction_p3, cache_protocol);
								}else if ( (found_c1 && (cache1[cache3_blk_addr].status == modified))    ||
									   (found_c2 && (cache2[cache3_blk_addr].status == modified))    ||
									   (found_c0 && (cache0[cache3_blk_addr].status == modified)) 
									)
								{
									write_back(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, current_instruction_p3, p3);
								}
								/* on both tranfers change the caches statuses */
								cache3[cache3_blk_addr].address = current_instruction_p3.address;
								cache3[cache3_blk_addr].status = shared;
								if (found_c1) { cache1[cache3_blk_addr].status = shared; }
								if (found_c2) { cache2[cache3_blk_addr].status = shared; }
								if (found_c0) { cache0[cache3_blk_addr].status = shared; }
								
							} else{
								/* memory access */
								memory_access_transfer(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, p3, current_instruction_p3.address);
								
								cache3[cache3_blk_addr].address = current_instruction_p3.address;
								cache3[cache3_blk_addr].status = exclusive;
								
							}
						} /* if (!found_p3)*/
						
						break;
					}
					case store:
					{
						/* if (cache hit), bus_clock += (1 cicle) */
						cache3_blk_addr = current_instruction_p3.address % CACHE_HEIGHT;
						found_p3 = false;
						found_c0 = false;
						found_c1 = false;
						found_c2 = false;
						found_c3 = false;
						if (cache3[cache3_blk_addr].status != invalid ) {
							if ( cache3[cache3_blk_addr].address == current_instruction_p3.address ){
								found_p3 = true;
								found_c3 = true; /* hit */
							}
						}
						else{
							if ( cache3[cache3_blk_addr].address == current_instruction_p3.address ){
								coer_misses_c3++;
							}
						}
						
						/* hit or miss costs 1 cycle */
						p3_clock += 1;
						if (found_p3) {
							if (cache3[cache3_blk_addr].status  == exclusive || cache3[cache3_blk_addr].status  == modified) {
								/* just overwrite the value without changing the status */
							}
							if (cache3[cache3_blk_addr].status  == shared) {
								if (cache_protocol == cache_invalidation){
									/* invalidate other cache lines and change local status from shared to modified */
									cache_access(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, p3, current_instruction_p3);
									
									if (cache1[cache3_blk_addr].status != invalid ) {
											if ( cache1[cache3_blk_addr].address == current_instruction_p3.address ){
													cache1[cache3_blk_addr].status = invalid;
											}
									}
									if (cache2[cache3_blk_addr].status != invalid ) {
											if ( cache2[cache3_blk_addr].address == current_instruction_p3.address ){
													cache2[cache3_blk_addr].status = invalid;
											}
									}
									if (cache0[cache3_blk_addr].status != invalid ) {
											if ( cache0[cache3_blk_addr].address == current_instruction_p3.address ){
													cache0[cache3_blk_addr].status = invalid;
											}
									}
									
									/* change the current status to exclusive */
									cache3[cache3_blk_addr].status = exclusive;
									cache3[cache3_blk_addr].address = current_instruction_p3.address;
								}
								else{
									/* Pay the broadcast */
									cache_transfer(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, p3, current_instruction_p3, cache_protocol);
								}
							}
						}
						
						if (!found_p3) {
							
							misses_c3++;
							
							/* cache access, costs 3 cycles, takes 2 bus cycles*/
							cache_access(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, p3, current_instruction_p3);
							
							if (cache1[cache3_blk_addr].status != invalid ) {
								if ( cache1[cache3_blk_addr].address == current_instruction_p3.address ){
									found_p3 = true;
									found_c1 = true;
								}
							}
							if (cache2[cache3_blk_addr].status != invalid ) {
								if ( cache2[cache3_blk_addr].address == current_instruction_p3.address ){
									found_p3 = true;
									found_c2 = true;
								}
							}
							if (cache0[cache3_blk_addr].status != invalid ) {
								if ( cache0[cache3_blk_addr].address == current_instruction_p3.address ){
									found_p3 = true;
									found_c0 = true;
								}
							}
							
							if (found_p3) {
								/* cache transfer */
								if ( (found_c1 && (cache1[cache3_blk_addr].status == exclusive)) ||
								     (found_c1 && (cache1[cache3_blk_addr].status == shared))    ||
								     (found_c2 && (cache2[cache3_blk_addr].status == exclusive)) ||
								     (found_c2 && (cache2[cache3_blk_addr].status == shared))    ||
								     (found_c0 && (cache0[cache3_blk_addr].status == exclusive)) ||
								     (found_c0 && (cache0[cache3_blk_addr].status == shared)) 
								   )
								{
									cache_transfer(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, p3, current_instruction_p3, cache_protocol);
								}else if ( (found_c1 && (cache1[cache3_blk_addr].status == modified))    ||
									   (found_c2 && (cache2[cache3_blk_addr].status == modified))    ||
									   (found_c0 && (cache0[cache3_blk_addr].status == modified)) 
									)
								{
									write_back(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, current_instruction_p3, p3);
								}
								if (cache_protocol == cache_invalidation){
									/* on both tranfers change the caches statuses */
									cache3[cache3_blk_addr].address = current_instruction_p3.address;
									cache3[cache3_blk_addr].status = modified;
									if (found_c1) { cache1[cache3_blk_addr].status = invalid; }
									if (found_c2) { cache2[cache3_blk_addr].status = invalid; }
									if (found_c0) { cache0[cache3_blk_addr].status = invalid; }
								}
								else{
									cache3[cache3_blk_addr].address = current_instruction_p3.address;
									
									if ((!found_c0) && (!found_c1) && (!found_c2))
										cache3[cache3_blk_addr].status = modified;
									else{
										cache3[cache3_blk_addr].status = shared;
										if (found_c0) {
											cache0[cache3_blk_addr].address = current_instruction_p3.address;
											cache0[cache3_blk_addr].status = shared; 
										}
										if (found_c1) {
											cache1[cache3_blk_addr].address = current_instruction_p3.address;
											cache1[cache3_blk_addr].status = shared; 
										}
										if (found_c3) {
											cache2[cache3_blk_addr].address = current_instruction_p3.address;
											cache2[cache3_blk_addr].status = shared; 
										}
									}
								}
							}else{
								/* memory access */
								memory_access_transfer(&bus_clock, &p3_clock, &lost_cycles_p3, bus, &bus_count, p3, current_instruction_p3.address);
								
								 cache3[cache3_blk_addr].address = current_instruction_p3.address;
								 cache3[cache3_blk_addr].status = exclusive;
								
							}
						} /* if (!found_p3) */
						break;
					}
					case add:
					case mul:
					case branch:
					{ /* do nothing, treated before */
							break;
					}
				}/* end switch */
			}
		}
		
		bus [ machine_clock % BUS_SIZE ].is_free = true;
		
		if(bus_count > 0){
			bus_count--;
		}
		
		machine_clock++;
		
	}
	
	dprintf(("--------------------------------------------------------------------\n" ));
	dprintf(("number of cycles: %d\n", machine_clock - 1));
	dprintf(("number of cycles to P0 finish its program quarter: %d\n", p0_clock ));
	dprintf(("number of cycles that P0 lost because of the bus: %d\n", lost_cycles_p0 ));
	dprintf(("number of misses in Cache0: %d\n", misses_c0));
	dprintf(("number of coerency misses in Cache0: %d\n", coer_misses_c0));
	dprintf(("number of cycles to P1 finish its program quarter: %d\n", p1_clock ));
	dprintf(("number of cycles that P1 lost because of the bus: %d\n", lost_cycles_p1 ));
	dprintf(("number of misses in Cache1: %d\n", misses_c1));
	dprintf(("number of coerency misses in Cache1: %d\n", coer_misses_c1));
	dprintf(("number of cycles to P2 finish its program quarter: %d\n", p2_clock ));
	dprintf(("number of cycles that P2 lost because of the bus: %d\n", lost_cycles_p2 ));
	dprintf(("number of misses in Cache2: %d\n", misses_c2));
	dprintf(("number of coerency misses in Cache2: %d\n", coer_misses_c2));
	dprintf(("number of cycles to P3 finish its program quarter: %d\n", p3_clock ));
	dprintf(("number of cycles that P3 lost because of the bus: %d\n", lost_cycles_p3 ));
	dprintf(("number of misses in Cache3: %d\n", misses_c3));
	dprintf(("number of coerency misses in Cache3: %d\n", coer_misses_c3));
	dprintf(("--------------------------------------------------------------------\n" ));
	
	return 0;
}

