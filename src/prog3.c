/***************************************************************************
 *   Copyright (C) 2009 by Alan Fischer e Silva / Leslie Harlley Watter    *
 *   watter@gmail.com                                                      *
 *   alanfischer85@gmail.com / alan@inf.ufpr.br                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "sim.h"

#define MATRIX_SIZE 20
#define ROWS        20

#define NUM_PROC 4

int main (int argc, char** argv){

	unsigned int D[MATRIX_SIZE][MATRIX_SIZE]; /* matriz de distancias */
	unsigned int startComp[NUM_PROC + 1] ={0,5,10,15,20};
	
	unsigned int i=0,j=0,k=0,t=0;

	instructions_t inst = load;
	processor_t p = p0;
	
	srand(time(NULL));

	/* Inicializa a matriz de distâncias*/
	for (i=0; i< MATRIX_SIZE;i++) {
		for (j=0; j< MATRIX_SIZE; j++) {
			if (i!=j) {
				D[i][j] = rand()%21;
				if (D[i][j] == 0 || D[i][j] == 20) {
					D[i][j] = 99;
				}
				D[j][i] = D[i][j];
			}else {
				D[i][j] = 0;
			}
		}
	}

	/* Imprime a matriz de distâncias, apenas para conferência */
	/*for (i=0; i< MATRIX_SIZE;i++) {
		for (j=0; j< MATRIX_SIZE; j++) {
			if (j==0) {
				printf("%2d ", i);
			}
			printf("%4d ", D[i][j]);
		}
		printf("\n");
	}
	
	printf("\n");*/
	

	for (t=0;t<NUM_PROC;t++) {
		
		/* Programa a ser simulado */
		for (k = startComp[t]; k < startComp[t+1]; k++){
			/* printf("add r0,r0 -> RegI \n");
			printf("add r0,r0 -> RegJ \n");
			printf("add r0,%2d -> RegK \n",ROWS); */
			switch (k){
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				{
					p = p0;
					inst = add;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					break;
				}
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				{
					p = p1;
					inst = add;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					break;
				}
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
				{
					p = p2;
					inst = add;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					break;
				}
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				{
					p = p3;
					inst = add;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					break;
				}
			}

			for (j = 0; j < ROWS; j++){
				for (i = 0; i < ROWS; i++){
					if (D[i][j] > D[i][k] + D[k][j])
						D[i][j] = D[i][k] + D[k][j];
					/* printf("----------------------------------------\n"); 
					printf("lw r1, D[%d] \nlw r2, D[%d]\nlw r3, D[%d] \n",(i*ROWS)+j,(i*ROWS)+k,(k*ROWS)+j);
					printf("add r2, r3 -> r4\n");
					printf("blte r1, r4, pulaIf\n",i,j);
					printf("sw D[%d],r4\n",(i*ROWS)+j);
					printf("pulaIf\n");
					printf("PROCESSOR: %2d\n", t);
					printf("ble RegI, RegM\n");
					printf("add r0,r0 -> RegI \n"); */
					
					
					switch (k){
						case 0:
						case 1:
						case 2:
						case 3:
						case 4:
						{
							p = p0;
							inst = load;
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+j);
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+k);
							printf("%02d:%02d:%d\n", p, inst, (k*ROWS)+j);
							inst = load;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = branch;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = store;
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+j);
							inst = branch;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = add;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							break;
						}
						case 5:
						case 6:
						case 7:
						case 8:
						case 9:
						{
							p = p1;
							inst = load;
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+j);
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+k);
							printf("%02d:%02d:%d\n", p, inst, (k*ROWS)+j);
							inst = load;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = branch;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = store;
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+j);
							inst = branch;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = add;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							break;
						}
						case 10:
						case 11:
						case 12:
						case 13:
						case 14:
						{
							p = p2;
							inst = load;
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+j);
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+k);
							printf("%02d:%02d:%d\n", p, inst, (k*ROWS)+j);
							inst = load;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = branch;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = store;
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+j);
							inst = branch;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = add;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							break;
						}
						case 15:
						case 16:
						case 17:
						case 18:
						case 19:
						{
							p = p3;
							inst = load;
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+j);
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+k);
							printf("%02d:%02d:%d\n", p, inst, (k*ROWS)+j);
							inst = load;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = branch;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = store;
							printf("%02d:%02d:%d\n", p, inst, (i*ROWS)+j);
							inst = branch;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							inst = add;
							printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
							break;
						}
					}
				}
				/* printf("add r0,r0 -> RegJ \n");
				printf("ble RegJ, RegM\n"); */
				switch (k){
					case 0:
					case 1:
					case 2:
					case 3:
					case 4:
					{
						p = p0;
						inst = add;
						printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
						inst = branch;
						printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
						break;
					}
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					{
						p = p1;
						inst = add;
						printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
						inst = branch;
						printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
						break;
					}
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
					{
						p = p2;
						inst = add;
						printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
						inst = branch;
						printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
						break;
					}
					case 15:
					case 16:
					case 17:
					case 18:
					case 19:
					{
						p = p3;
						inst = add;
						printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
						inst = branch;
						printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
						break;
					}
				}
			}
			/* printf("add r0,r0 -> RegK \n");
			printf("====???ble %2d, %2d\n",k,startComp[t+1]); */
			switch (k){
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				{
					p = p0;
					inst = add;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					inst = branch;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					break;
				}
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				{
					p = p1;
					inst = add;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					inst = branch;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					break;
				}
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
				{
					p = p2;
					inst = add;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					inst = branch;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					break;
				}
				case 15:
				case 16:
				case 17:
				case 18:
				case 19:
				{
					p = p3;
					inst = add;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					inst = branch;
					printf("%02d:%02d:%d\n", p, inst, INVALID_ADDRESS);
					break;
				}
			}
		}
	}
	
	return 0;
}
