/***************************************************************************
 *   Copyright (C) 2009 by Alan Fischer e Silva / Leslie Harlley Watter    *
 *   watter@gmail.com                                                      *
 *   alanfischer85@gmail.com / alan@inf.ufpr.br                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "sim.h"

program_t* read_program (char *filename, unsigned int *program_size){
	FILE *fd;
	char line[LINE_SIZE];
	char delim[] = ":";
	char *result = NULL;
	unsigned int  i, j;
	bool first = true;
	program_t *program;
	
	fd = fopen (filename, "r");
	
	if(fd){
		j = 0;
		while((fgets(line, LINE_SIZE, fd)) != NULL) {
			if (first){
				*program_size = strtoul(strdup(line), NULL, DEC_BASE);
				if ((*program_size) > 0){
					program = (program_t *) malloc (sizeof(program_t) * (*program_size));
					first = false;
				}
				else{
					perror("Program size must be greater than 0");
					exit(1);
				}
			}
			else{
				result = strtok(line, delim);
				i = 0;
				while(result != NULL){
					switch (i){
						case 0:
							program[j].processor = atoi(result);
							break;
						case 1:
							program[j].instr_type = atoi(result);
							break;
						case 2:
							program[j].address = atoi(result);
							break;
						default:
							printf("Error while reading program file\n");
							break;
					}
					i++;
					result = strtok(NULL, delim);
				}
				j++;
			}
		}
	}
	else{
		printf ("Error opening file %s\n", filename);
		exit(1);
	}
	
	return program;
}

void alloc_queues(processor_fifo_t *fifo_p0, processor_fifo_t *fifo_p1, processor_fifo_t *fifo_p2, 
					processor_fifo_t *fifo_p3, unsigned int size){
	fifo_p0 = (processor_fifo_t *) malloc (sizeof(processor_fifo_t) * size);
	fifo_p1 = (processor_fifo_t *) malloc (sizeof(processor_fifo_t) * size);
	fifo_p2 = (processor_fifo_t *) malloc (sizeof(processor_fifo_t) * size);
	fifo_p3 = (processor_fifo_t *) malloc (sizeof(processor_fifo_t) * size);
}

void enqueue (processor_fifo_t *fifo_p, instructions_t instr_type, unsigned int address, unsigned int *top){
	fifo_p[*top].instr_type = instr_type;
	fifo_p[*top].address = address;
	(*top)++;
}

void select_enqueue(program_t program, processor_fifo_t *fifo_p0, processor_fifo_t *fifo_p1, processor_fifo_t *fifo_p2,
					processor_fifo_t *fifo_p3, unsigned int *top0, unsigned int *top1, unsigned int *top2, 
					unsigned int *top3){
	
	switch(program.processor){
		case p0:
		{
			enqueue(fifo_p0, program.instr_type, program.address, top0);
			break;
		}
		case p1:
		{
			enqueue(fifo_p1, program.instr_type, program.address, top1);
			break;
		}
		case p2:
		{
			enqueue(fifo_p2, program.instr_type, program.address, top2);
			break;
		}
		case p3:
		{
			enqueue(fifo_p3, program.instr_type, program.address, top3);
			break;
		}
	}
}

void unqueue (processor_fifo_t *fifo_p, unsigned int *top, unsigned int size){
	if (*top >= size){
		;
	}
	else{
		(*top)++;
	}
}


unsigned int  time_instruction(instructions_t instr_type){
	unsigned int time_inst = 0;
	
	
	/* loads and stores cannot be computed simply like this */
	switch (instr_type) {
		case add:
		{
			time_inst = ula_arit;
			break;
		}
		case mul:
		{
			time_inst = ula_mul;
			break;
		}
		case branch:
		{
			time_inst = ula_branch;
			break;
		}
		case load:
		case store:
		{
			break;
		}
	}
	return time_inst;
}

/* used to do the 3 cycles operations, first cycle - bus arb. (do not use the bus) */
unsigned int free_bus_position_ask(unsigned int bus_clock, bus_t bus[]){
	while ( (bus[ ((bus_clock + 1) % BUS_SIZE) ].is_free == false) && 
		(bus[ ((bus_clock + 2) % BUS_SIZE) ].is_free == false) ){ 
		bus_clock = bus_clock + 1; 
	}
	
	bus[ ((bus_clock + 1) % BUS_SIZE) ].is_free = false;
	bus[ ((bus_clock + 2) % BUS_SIZE) ].is_free = false;
	
	return bus_clock;
}

/* used to do the 8 cycles transfers */
unsigned int free_bus_position_transfer(unsigned int bus_clock, bus_t bus[]){
	while ( (bus[ (bus_clock % BUS_SIZE) ].is_free == false) &&
		(bus[ ((bus_clock + 1) % BUS_SIZE) ].is_free == false) &&
		(bus[ ((bus_clock + 2) % BUS_SIZE) ].is_free == false) &&
		(bus[ ((bus_clock + 3) % BUS_SIZE) ].is_free == false) && 
		(bus[ ((bus_clock + 4) % BUS_SIZE) ].is_free == false) &&
		(bus[ ((bus_clock + 5) % BUS_SIZE) ].is_free == false) &&
		(bus[ ((bus_clock + 6) % BUS_SIZE) ].is_free == false) &&
		(bus[ ((bus_clock + 7) % BUS_SIZE) ].is_free == false) ){
		bus_clock = bus_clock + 1; 
	}
	
	bus[ ((bus_clock    ) % BUS_SIZE) ].is_free = false;
	bus[ ((bus_clock + 1) % BUS_SIZE) ].is_free = false;
	bus[ ((bus_clock + 2) % BUS_SIZE) ].is_free = false;
	bus[ ((bus_clock + 3) % BUS_SIZE) ].is_free = false;
	bus[ ((bus_clock + 4) % BUS_SIZE) ].is_free = false;
	bus[ ((bus_clock + 5) % BUS_SIZE) ].is_free = false;
	bus[ ((bus_clock + 6) % BUS_SIZE) ].is_free = false;
	bus[ ((bus_clock + 7) % BUS_SIZE) ].is_free = false;
	
	return bus_clock;
}

/* used to increment the bus_counter according to bus usage*/
unsigned int bus_counter_inc(unsigned int bus_count, unsigned int increment){
	
	if ( (bus_count + increment) < BUS_SIZE ){
		bus_count += increment; 
	}
	else{
		bus_count = (bus_count + increment) % BUS_SIZE; 
	}
	
	return bus_count;
}

/* used to get the bus position according to the bus SIZE */
unsigned int bus_pos(unsigned int bus_clock){
	return (bus_clock % BUS_SIZE);
}

void cache_access(machine_clock_t *bus_clock, machine_clock_t *p_clock, unsigned int *lost_cycles_p, 
		  bus_t *bus, unsigned int *bus_count, processor_t p, processor_fifo_t current_instruction_p){
	
	unsigned int wait_for_bus;
	
	wait_for_bus = *bus_clock;
	
	*bus_clock = free_bus_position_ask(*bus_clock, bus);
	wait_for_bus = *bus_clock - wait_for_bus;
	
	/* adding the cycles waited for the bus, it might be buzy */
	*p_clock = *p_clock + wait_for_bus;
	*lost_cycles_p += wait_for_bus;
	
	/* bus reserve, time to look in the caches - 3 cycles - 2 using the bus*/
	*p_clock += 3;
	*lost_cycles_p += 2;
	(*bus_clock) += 2;
	*bus_count = bus_counter_inc( *bus_count, 2 );
	
	return;
}

void cache_transfer(machine_clock_t *bus_clock, machine_clock_t *p_clock, unsigned int *lost_cycles_p, 
					 bus_t *bus, unsigned int *bus_count, processor_t p, processor_fifo_t current_instruction_p,
					 cache_protocol_t cache_protocol){
	
	unsigned int i = 0;
	unsigned int wait_for_bus = 0;
	
	switch(current_instruction_p.instr_type){
		case load:
		{
			/* Load operations */
			/* Bus Arbitration + Tag Check == incremented by cache_access */
			
			wait_for_bus = *bus_clock;
			
			/* (16 words -- 2 per time) */
			*bus_clock = free_bus_position_transfer(*bus_clock, bus);
			
			/* wait for bus to transfer */
			wait_for_bus = *bus_clock - wait_for_bus;
			*p_clock += wait_for_bus;
			*lost_cycles_p += wait_for_bus;
			*bus_count = bus_counter_inc( *bus_count, wait_for_bus );
			
			/* cycles before transfer - total will be 10 - 2 + 8 */
			*p_clock += 2;
			*lost_cycles_p += 2;
			*bus_count = bus_counter_inc( *bus_count, 2 );
			
			for (i = 0; i < CACHE_WORDS; i+=BUS_TRANSFER_WORDS) {
				bus[ bus_pos(*bus_clock) ].is_free = false;
				/* each false increments bus_count */
				*bus_count = bus_counter_inc( *bus_count, 1 );
				
				/* incrementing the p_clock because were spent 1 cycle tranfering 2 blocks */
				*p_clock += 1;
				*lost_cycles_p += 1;
				
			}
			break;
		}
		case store:
		{
		
			/* Store operations */
			/* Bus Arbitration + Tag Check == incremented by cache_access */
			
			/* check whether protocol to use */
			if (cache_protocol == cache_invalidation) {
				/* invalidation */
				wait_for_bus = *bus_clock;
				
				/* (16 words -- 2 per time) */
				*bus_clock = free_bus_position_ask(*bus_clock, bus);
				wait_for_bus = *bus_clock - wait_for_bus;
				
				/* adding the cycles waited for the bus */
				*p_clock += wait_for_bus;
				*lost_cycles_p += wait_for_bus;
				*bus_count = bus_counter_inc( *bus_count, wait_for_bus );
				
				/* cycles to invalidate - 3 cycles to invalidate - 2 using bus */
				*p_clock += 3;
				*lost_cycles_p += 2;
				*bus_count = bus_counter_inc( *bus_count, 2 );
			}
			else{
				/* update */
				wait_for_bus = *bus_clock;
				/* Tag + Answer - 2 cycles */
				*bus_clock = free_bus_position_ask(*bus_clock, bus);
				wait_for_bus = *bus_clock - wait_for_bus;
				/* adding the cycles waited for the bus */
				*p_clock += wait_for_bus;
				*lost_cycles_p += wait_for_bus;
				
				/* cycles before transfer - total will be 10 - 2 + 8 */
				/* was discussed in class, update takes 10 cycle, first
				2 of invalidation + 8 for transfer. The first cycle does
				not use the bus*/
				*p_clock += 2;
				*lost_cycles_p += 1;
				*bus_count = bus_counter_inc( *bus_count, 1 );
				
				
				/* need to find 8 cycles to do the transfer */
				wait_for_bus = *bus_clock;
				
				/* 16 words - 2 words per cycle - 8 cycles */
				*bus_clock = free_bus_position_transfer(*bus_clock, bus);
				wait_for_bus = *bus_clock - wait_for_bus;
				
				/* adding the cycles waited for the bus */
				*p_clock += wait_for_bus;
				*lost_cycles_p += wait_for_bus;
				
				for (i = 0; i < CACHE_WORDS; i+=BUS_TRANSFER_WORDS) {
					bus[ bus_pos(*bus_clock) ].is_free = false;
					/* each false increments bus_count */
					*bus_count = bus_counter_inc( *bus_count, 1 );
					
					*p_clock += 1;
					*lost_cycles_p += 1;
				}
			}
			break;
		}
		case add:
		case mul:
		case branch:
			break;
	}
	return;
}

void memory_access_transfer(machine_clock_t *bus_clock, machine_clock_t *p_clock, unsigned int *lost_cycles_p, 
							 bus_t bus[], unsigned int *bus_count, processor_t p, unsigned int address){
	
	unsigned int i = 0;
	unsigned int wait_for_bus = 0;
	/* Bus */
	
	/* Next 19 cycles do not matter, memory accesss */
	*bus_clock += 19;
	*p_clock += 19;
	*lost_cycles_p += 19;
	*bus_count = bus_counter_inc( *bus_count, 19 );
	
	/* Find 8 cycles to do the transfer */
	wait_for_bus = *bus_clock;
	*bus_clock = free_bus_position_transfer(*bus_clock, bus);
	wait_for_bus = *bus_clock - wait_for_bus;
	
	/* adding the cycles waited for the bus */
	*p_clock += wait_for_bus;
	*lost_cycles_p += wait_for_bus;
	*bus_count = bus_counter_inc( *bus_count, wait_for_bus );
	
	for (i = 0; i < CACHE_WORDS; i+=BUS_TRANSFER_WORDS) {
		bus[ bus_pos(*bus_clock) ].is_free = false;
		/* each false increments bus_count */
		*bus_count = bus_counter_inc( *bus_count, 1 );
		
		/* incrementing the p_clock because were spent 1 cycle tranfering 2 blocks */
		*p_clock = *p_clock + 1;
		*lost_cycles_p = *lost_cycles_p + 1;
		
	}
	
	return;
}

void write_back(machine_clock_t *bus_clock, machine_clock_t *p_clock, unsigned int *lost_cycles_p, bus_t *bus, unsigned int *bus_count, processor_fifo_t current_instruction_p, processor_t p){
	
	unsigned int i = 0;
	unsigned int wait_for_bus = 0;
	
	/* write back */
	wait_for_bus = *bus_clock;
	
	/* Tag + Answer - 2 cycles */
	*bus_clock = free_bus_position_ask(*bus_clock, bus);
	wait_for_bus = *bus_clock - wait_for_bus;
	
	/* adding the cycles waited for the bus - first cycle does
	not use the bus */
	*p_clock += wait_for_bus;
	*lost_cycles_p += wait_for_bus;
	*p_clock += 3;
	*lost_cycles_p += 2;
	
	wait_for_bus = *bus_clock;
	/* 16 words - 2 words per cycle - 8 cycles */
	*bus_clock = free_bus_position_transfer(*bus_clock, bus);
	wait_for_bus = *bus_clock - wait_for_bus;
	
	/* adding the cycles waited for the bus */
	*p_clock += wait_for_bus;
	*lost_cycles_p += wait_for_bus;
				
	for (i = 0; i < CACHE_WORDS; i+=BUS_TRANSFER_WORDS) {
		bus[ bus_pos(*bus_clock) ].is_free = false;
		/* each false increments bus_count */
		*bus_count = bus_counter_inc( *bus_count, 1 );
		
		*p_clock += 1;
		*lost_cycles_p += 1;
		
	}
	
	/* Next 12 cycles do not matter, memory accesss -> used 8 cycles to transfer data to cache */
	*bus_clock += 12;
	*p_clock += 12;
	*lost_cycles_p += 12;
	*bus_count = bus_counter_inc( *bus_count, 12 );
	
	/* Find 8 cycles to do the transfer */
	wait_for_bus = *bus_clock;
	*bus_clock = free_bus_position_transfer(*bus_clock, bus);
	wait_for_bus = *bus_clock - wait_for_bus;
	
	/* adding the cycles waited for the bus */
	*p_clock += wait_for_bus;
	*lost_cycles_p += wait_for_bus;
	*bus_count = bus_counter_inc( *bus_count, wait_for_bus );
	
	for (i = 0; i < CACHE_WORDS; i+=BUS_TRANSFER_WORDS) {
		bus[ bus_pos(*bus_clock) ].is_free = false;
		/* each false increments bus_count */
		*bus_count = bus_counter_inc( *bus_count, 1 );
		
		/* incrementing the p0_clock because were spent 1 cycle tranfering 2 blocks */
		*p_clock = *p_clock + 1;
		*lost_cycles_p = *lost_cycles_p + 1;
	}
	
	return;
}


