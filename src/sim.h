/***************************************************************************
 *   Copyright (C) 2009 by Alan Fischer e Silva / Leslie Harlley Watter    *
 *   watter@gmail.com                                                      *
 *   alanfischer85@gmail.com / alan@inf.ufpr.br                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


/*
Endereços requisitados no barramento -> memória sempre existem na memória

Perguntas:

Situação:

tenho um elemento na cache ind[x] que está sujo
peço um endereco y no barramento e recebo retorno do endereço


*/

#ifndef SIM_H
#define SIM_H


#define DEBUG

#ifdef DEBUG
#   define dprintf(args) printf args
#else
#   define dprintf(args) do { } while(0)
#endif


//#define ERROR

#ifdef ERROR
#   define eprintf(args) printf args
#else
#   define eprintf(args) do { } while(0)
#endif




#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define CACHE_WORDS 16
#define CACHE_HEIGHT 1024
#define BUS_SIZE 16384 
#define LINE_SIZE 80
#define DEC_BASE 10
#define BUS_TRANSFER_WORDS 2 

#define PROCESSORS_NUM 4
#define INVALID_PROCESSOR 9
#define INVALID_ADDRESS   -2

#define PROG1_SIZE 4000
#define PROG2_SIZE 33600
#define PROG3_SIZE 64900

/* clock */
typedef unsigned  int machine_clock_t;

typedef enum __cache_protocol{
	cache_invalidation = 0,
	cache_update
}cache_protocol_t;

typedef enum __bus_operation{
	donothing = 0,
	invalidation,
	update
}bus_operation_t;

typedef enum __cache_status{
	none = 0,
	modified,
	exclusive,
	shared,
	invalid
}cache_status_t;

typedef enum __processor_instructions{
	load  = 0,
	store = 1,
	add   = 2,
	mul   = 3,
	branch = 4
}instructions_t;

typedef enum __processor{
	p0 = 0,
	p1,
	p2,
	p3
}processor_t;

typedef enum __instr_time{
	ula_arit = 5,
	ula_branch = 4,
	ula_mul = 10,
	mem_load = 3,
	mem_store = 30,
	cache_read_local = 1,
	cache_read_remote = 3
}instr_time_t;

typedef struct __program{
	processor_t processor;
	instructions_t instr_type;
	unsigned int address;
}program_t;

typedef struct __processor_fifo{
	instructions_t instr_type;
	unsigned int address;
}processor_fifo_t;

typedef struct __cache_block {
	cache_status_t status;
	unsigned int address;
}cache_block_t;


typedef struct __bus_block {
	unsigned int address;
	unsigned int status;
}bus_block;

typedef struct __bus {
	bool is_free;
}bus_t;

typedef struct __memory {
	unsigned int address;
}memory_t;

program_t* read_program (char *filename, unsigned int *program_size);


void alloc_queues(processor_fifo_t *fifo_p0, processor_fifo_t *fifo_p1, processor_fifo_t *fifo_p2, 
					processor_fifo_t *fifo_p3, unsigned int size);

void enqueue(processor_fifo_t *fifo_p, instructions_t instr_type, unsigned int address, unsigned int *top);

void select_enqueue(program_t program, processor_fifo_t *fifo_p0, processor_fifo_t *fifo_p1, processor_fifo_t *fifo_p2,
					processor_fifo_t *fifo_p3, unsigned int *top0, unsigned int *top1, unsigned int *top2, 
					unsigned int *top3);

void unqueue(processor_fifo_t *fifo_p, unsigned int *top, unsigned int size);

unsigned int time_instruction(instructions_t instr_type);

unsigned int free_bus_position_ask(unsigned int bus_clock, bus_t bus[]);  

unsigned int free_bus_position_transfer(unsigned int bus_clock, bus_t bus[]);

unsigned int bus_counter_inc(unsigned int bus_count, unsigned int increment);

unsigned int bus_pos(unsigned int bus_clock);

void cache_access(machine_clock_t *bus_clock, machine_clock_t *p_clock, unsigned int *lost_cycles_p, 
				   bus_t *bus, unsigned int *bus_count, processor_t p, processor_fifo_t current_instruction_p);

void cache_transfer(machine_clock_t *bus_clock, machine_clock_t *p_clock, unsigned int *lost_cycles_p, 
					bus_t *bus, unsigned int *bus_count, processor_t p, processor_fifo_t current_instruction_p, 
					cache_protocol_t cache_protocol);

void memory_access_transfer(machine_clock_t *bus_clock, machine_clock_t *p_clock, unsigned int *lost_cycles_p, 
							bus_t *bus, unsigned int *bus_count, processor_t p, unsigned int address);

void write_back(machine_clock_t *bus_clock, machine_clock_t *p_clock, unsigned int *lost_cycles_p, bus_t *bus, unsigned int *bus_count, processor_fifo_t current_instruction_p, processor_t p);

#endif
