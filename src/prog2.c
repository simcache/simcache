/***************************************************************************
 *   Copyright (C) 2009 by Alan Fischer e Silva / Leslie Harlley Watter    *
 *   watter@gmail.com                                                      *
 *   alanfischer85@gmail.com / alan@inf.ufpr.br                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sim.h"


#define MATRIX_SIZE 16

int main (int argc, char** argv){

	unsigned int matA[MATRIX_SIZE][MATRIX_SIZE]; /* matrix A */
	unsigned int matB[MATRIX_SIZE][MATRIX_SIZE]; /* matrix B */
	unsigned int matR[MATRIX_SIZE][MATRIX_SIZE]; /* result matrix */

	unsigned int i=0,j=0,k=0;
	unsigned int temp=0;
	
	instructions_t inst = load;
	processor_t p = p0;


	/* initializing matrixes */
	for (i=0; i< MATRIX_SIZE;i++) {
		for (j=0; j< MATRIX_SIZE; j++) {
			matA[i][j] = rand();
			matB[i][j] = rand();
			matR[i][j] = 0;
		}
	}
	
	inst = add;



        /* FIXME: fazer a blocagem aqui em cima, precisarei inicializar  
	   as variáveis i,j e k diferentemente para cada processador */


/* 	printf("add r0,r0 -> RegI \n"); */
/* 	printf("add r0,r0 -> RegJ \n"); */
/* 	printf("add r0,r0 -> RegK \n"); */
/* 	printf("addi r0,%2d -> RegM \n", MATRIX_SIZE); */

	/* p0 */ 
	p=p0;
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, MATRIX_SIZE);

	/* p1 */ 
	p=p1;
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, MATRIX_SIZE);


	/* p2 */ 
	p=p2;
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, MATRIX_SIZE);

	/* p3 */ 
	p=p3;
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, 0);
	printf("%02d:%02d:%d\n",  p,inst, MATRIX_SIZE);


/* address 00000 is added to instructions that have no address on bus */
	
	/* doing the matrix multiplication */
	for (i=0; i< MATRIX_SIZE;i++) {
		switch (i) {
		case 0:
		case 1:
		case 2:
		case 3:
		{
			p=p0;
			break;
		}
		case 4:
		case 5:
		case 6:
		case 7:
		{
					
			p=p1;
			break;
		}
		case 8:
		case 9:
		case 10:
		case 11:
		{
					
			p=p2;
			break;
		}
		case 12:
		case 13:
		case 14:
		case 15:
		{
					
			p=p3;
			break;
		}
				
		} /* end switch */

		for (j=0; j< MATRIX_SIZE; j++) {
			for (k=0; k< MATRIX_SIZE; k++) {
				temp = matA[i][k] * matB[k][j];
				matR[i][j] = matR[i][j] + temp;
/* 				printf("----------------------------------------\n"); */
/* 				printf("lw A[%2d] -> RegA \n",(i*MATRIX_SIZE)+k); */
/* 				printf("lw T[%2d] -> RegB \n",(j*MATRIX_SIZE)+k); */
/* 				printf("mult RegA, RegB -> RegC\n"); */
/* 				printf("lw R[%2d] -> RegR\n",i*MATRIX_SIZE+j); */
/* 				printf("add RegR,RegC -> RegR\n"); */
/* 				printf("sw RegR -> R[%2d]\n",i*MATRIX_SIZE+j); */
/* 				printf("PROCESSOR: %2d\n", i%4); */
/* 				printf("ble RegK, RegM\n"); */
/* 				printf("add RegK,1 -> RegK\n"); */
				inst = load;
				printf("%02d:%02d:%d\n",  p,inst, (i*MATRIX_SIZE)+k);
				printf("%02d:%02d:%d\n",  p,inst, (j*MATRIX_SIZE)+k);
				printf("%02d:%02d:%d\n",  p,inst, (i*MATRIX_SIZE)+j);
				inst = mul;
				printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);
				inst = add;
				printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);
				inst = store;
				printf("%02d:%02d:%d\n",  p,inst, (i*MATRIX_SIZE)+j);
				inst = add; /* incrementing k */
				printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);
				inst = branch;
				printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);
			}

/* 			printf("add r0,r0 -> RegK \n"); */
/* 			printf("add RegJ,1 -> RegJ\n"); */
/* 			printf("ble RegJ, RegM\n"); */

			inst = add; /* setting k to 0 */
			printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);

			inst = add; /* incrementing j */
			printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);

			inst = branch;
			printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);

		}
/* 		printf("add r0,r0 -> RegJ \n"); */
/* 		printf("add RegI,1 -> RegI\n"); */
/* 		printf("ble RegI, RegM\n"); */

		inst = add; /* setting j to 0 */
		printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);

		inst = add; /* incrementing i */
		printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);

		inst = branch;
		printf("%02d:%02d:%d\n",  p,inst, INVALID_ADDRESS);

	}

	return 0;
}
